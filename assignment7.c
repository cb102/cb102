//Enter your code here
#include<stdio.h>
int main()
{
    int arr[25][25],n,i,j,dsum=0,udsum=0,ldsum=0;
    printf("Enter the size of the nxn matrix\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            scanf("%d",&arr[i][j]);
        }
    }
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            if(i==j)
            dsum+=arr[i][j];
            else if(i<j)
            udsum+=arr[i][j];
            else if(i>j)
            ldsum+=arr[i][j];
        }
    }
    printf("Array:\n");
    for(i=0;i<n;i++)
    {
        for(j=0;j<n;j++)
        {
            printf("%d ",arr[i][j]);
        }
        printf("\n");
        
    }
    printf("Sum of diagonal elements : %d\nSum of elements above the diagonal : %d\nSum of elements below the diagonal : %d",dsum,udsum,ldsum);
    return 0;
}