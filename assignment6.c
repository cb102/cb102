//Enter your code here
#include <stdio.h>

int main()
{
    int arr[100],n,odd[100],even[100],o=0,e=0,i;
    printf("Enter the size of the array(max:100)\n");
    scanf("%d",&n);
    printf("Enter the elements of the array\n");
    for(i=0;i<n;i++)
    {
        scanf("%d",&arr[i]);
        if(arr[i]%2==0)
        {
            even[e]=arr[i];
            e++;
        }
        else
        {    
            odd[o]=arr[i];
            o++;
        }

    }
    printf("Even Array:\n");
    for(i=0;i<e;i++)
    printf("%d ",even[i]);
    printf("\nOdd Array:\n");
    for(i=0;i<o;i++)
    printf("%d ",odd[i]);
    return 0;
}